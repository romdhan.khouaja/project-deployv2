<?php
namespace Deployer;

require 'recipe/symfony.php';

// Project name
set('application', 'project-deployv2');
set('http_user', NULL);

// Project repository
set('repository', 'https://gitlab.com/romdhan.khouaja/project-deployv2.git');

// Nombre de déploiements à conserver avant de les supprimer.
set('keep_releases', 4);
set('deploy_path', '/cygdrive/c/xampp/htdocs');
set('release_path', '/cygdrive/c/xampp/htdocs/{{application}}');
set('bin_dir', 'bin');
set('clear_paths', ['var/cache']);

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', false);
set('check_remote_head', false);
// git_recursive
set('git_recursive', false);
// ssh_multiplexing
set('ssh_multiplexing', false);

// Shared files/dirs between deploys 
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server 
add('writable_dirs', []);


// Hosts 
host('project.com')
    ->hostname('client1')
    ->set('deploy_path', '~/{{application}}');

// Tasks
// tache de deployement installation des dependances avec composer
task('deploy:vendors', function () {
    $result = run('cd {{release_path}} ; composer install --verbose --prefer-dist --no-progress --no-interaction --optimize-autoloader');
    writeln("installation avec composer : $result");
});

task('deploy:prepare_first', function () {
    write("{{release_path}}");
    run('cd {{deploy_path}}; rm -Rf  {{application}}');
});

task('deploy:assets:install', function () {
    $result = "ok";
    writeln("installation des assets : $result");
});
task('deploy:writable ', function () {
    $result = "ok";
    writeln("configuration writable : $result");
}); 
task('deploy:cache:clear', function () {
    $result = run('cd {{release_path}} ; chmod +x bin/console; php bin/console cache:clear');
    writeln("effacement du cache : $result");
});
task('deploy:cache:warmup', function () {
    $result = run('cd {{release_path}} ; chmod +x bin/console; php bin/console cache:warmup');
    writeln("effacement du cache : $result");
});
task('build', function () {
    run('cd {{release_path}} && build');
});

// make migration
task('db:make:migration', function () {
    $result = run('cd {{release_path}} ; php bin/console make:migration');
    writeln("creation du fichier de migration: $result");
});
// doctrine migrations migrate
task('db:doctrine:migrations:migrate', function () {
    $result = run('cd {{release_path}} ; php bin/console doctrine:migrations:migrate -n');
    writeln("migration de la base de donnees : $result");
});
// doctrine:database:drop
task('db:doctrine:database:drop', function () {
    $result = run('cd {{release_path}} ; php bin/console doctrine:database:drop --force');
    writeln("suppression de la base de donnees : $result");
});
// doctrine:database:drop
task('db:doctrine:database:create', function () {
    $result = run('cd {{release_path}} ; php bin/console doctrine:database:create');
    writeln("creation de la base de donnees : $result");
});
// visualiser toutes les anciens fichiers de migrations
task('show:all:migrations', function () {
    $result = run('cd {{release_path}} ; ls -l src/Migrations/');
    writeln("listes des fichiers de migrations : $result");
});
// suppression des anciens fichiers migrations
task('db:delete:all:migrations', function () {
    $result = run('cd {{release_path}} ; ls -l src/Migrations; rm -rf src/Migrations/*');
    writeln("suppression des fichiers de migrations : $result");
});

// deploy done
task('deploy:done', function () {
    write('Deploy done!');
});

after('deploy', 'deploy:done');

// deploy info done 
task('deploy:info:done', function () {
    write('Deploy info done!');
});

after('deploy:info', 'deploy:info:done');

// deploy 'deploy:prepare_first' done
task('deploy:prepare_first:done', function () {
    write('Deploy prepare_first done!');
});

after('deploy:prepare_first', 'deploy:prepare_first:done');

// deploy 'deploy:symlink' done
task('deploy:symlink:done', function () {
    write('Deploy symlink done!');
});

after('deploy:symlink', 'deploy:symlink:done');

// deploy 'deploy:unlock' done
task('deploy:unlock:done', function () {
    write('Deploy unlock done!');
});

after('deploy:unlock', 'deploy:unlock:done');

task('deploy', [
    'deploy:info',
    'deploy:prepare_first',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:clear_paths',
    'deploy:shared',
    'deploy:vendors',
    'deploy:cache:clear',
    'deploy:cache:warmup',
    // 'deploy:writable',
    'deploy:assets:install',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
])->desc('Deploiement du projet');

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

// before('deploy:symlink', 'database:migrate');

